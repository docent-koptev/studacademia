# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from django.views.generic import TemplateView

urlpatterns = patterns('indexpage.views',
    url(r'^$', 'show_index_page', name='home'),
    url(r'^ajax/$', 'send_email_ajax', name='ajax_email'),
    url(r'^order/(?P<form_name>[-\w]+)/$', 'show_form', name='show_form'),
    url(r'^order/studwork/$', 'show_form', name='show_work_form'),
    url(r'^page/(?P<slug>[-\w]+)/$', 'show_bottom_page', name='menu'),
    url(r'^contacts/$', TemplateView.as_view(template_name='contacts.html'), name="contacts"),
    url(r'^getjob/(?P<form_attr>[-\w]+)/$','show_vakansy_form', name="getjob"),
    url(r'^robots\.txt$', TemplateView.as_view(template_name='robots.txt', content_type='text/plain')),
)
