# -*- coding: utf-8 -*-
# django imports
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site
from django.core.mail import EmailMultiAlternatives
from django.contrib.auth.models import User


superuser_email = User.objects.get(pk=1,is_superuser=1).email
def send_vakansy():
    subject = u"Поступил заказ на вакансию"
    from_email = 'from@studacademia.ru'
    to = [superuser_email] 
    text_content = 'This is an important message.'
    html_content = '<p>This is an <strong>important</strong> message.</p>'

    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()