# -*- coding: utf-8 -*-
from django import template
from django.shortcuts import render
from django.shortcuts import render_to_response,render
from models import Uslugi,Menubottom,StudWork,Slider
from django.http import HttpResponse
from django.core.mail import send_mail
from forms import UpgradeUnicForm,ElectroDocumentForm,TranslateBlankForm,StudWorkForm,RepetitorForm,TranslateForm,AuthorForm,ManagerForm
from django.contrib.auth.models import User
from mail import send_vakansy
from django.contrib.sites.models import Site


domain = Site.objects.get_current().domain
#show index page
def show_index_page(request):
	uslugi_arr = Uslugi.objects.all().order_by('id')
	slider = Slider.objects.filter(display=True,show_on='main')
	return render_to_response("index.html",{'uslugi':uslugi_arr,'slider':slider,}, context_instance=template.RequestContext(request))


def send_email_ajax(request):
	superuser_email = User.objects.get(pk=1,is_superuser=1).email
	error = True
	if request.method == 'GET':
		name = request.GET['name']
		if name == u'Ваше имя?':
			name = ''
		phone = request.GET['phone']
		if phone == u'Номер телефона (+7ХХХХХХХ)':
			phone = ''
		subject = request.GET['subject']
		if (subject!='' and phone!='' and name!=''):
			error = False
			body = u'Необходимо оценить работу: ' + subject + '\n'
			body += u"Имя: " + name + '\n'
			body += u"Телефона: " + phone + '\n'
		else:
			error = True
			return HttpResponse('error')

		if not error:
			send_mail('Оценка работы',body, 'from@example.com',[superuser_email], fail_silently=False)
			return HttpResponse('ok')

def show_bottom_page(request,slug):
	item = Menubottom.objects.get(slug=slug)
	return render_to_response('alone.html',{'item':item,},context_instance=template.RequestContext(request))


def show_form(request,form_name):
	superuser_email = User.objects.get(pk=1,is_superuser=1).email
	if request.method == 'POST':
		if (form_name == 'studwork'):
			form = StudWorkForm(request.POST,request.FILES)
			zakaz_name = u'Учебная работа'
		if (form_name =='blank'):
			form = TranslateBlankForm(request.POST,request.FILES)
			zakaz_name = u'Бланк перевода'
		if (form_name =='upgradeunic'):
			form = UpgradeUnicForm(request.POST,request.FILES)
			zakaz_name = u'Повышение уникальности'
		if (form_name =='electro'):
			form = ElectroDocumentForm(request.POST,request.FILES)
			zakaz_name = u'Электронный документ'
		if form.is_valid():
			if form.is_valid():
				new_author = form.save(commit=False)
				new_author.save()
				form.save_m2m()
				url =  unicode(new_author.get_absolute_url())
				body =  u'На сайте поступил новый заказ.\n'
				per = u'перейти'
				body += 'http://'+domain+'/admin/indexpage' + url
				send_mail(u'На сайте поступил заказ: ' + zakaz_name,body, 'from@studacademia.ru',[superuser_email], fail_silently=False)
				return render_to_response("order.html",{'thanks':True}, context_instance=template.RequestContext(request))
	else:
		if (form_name == 'studwork'):
			form = StudWorkForm()
		if (form_name =='blank'):
			form = TranslateBlankForm()
		if (form_name =='upgradeunic'):
			form = UpgradeUnicForm()
		if (form_name =='electro'):
			form = ElectroDocumentForm()
	return render_to_response("order.html",{'form':form,}, context_instance=template.RequestContext(request))



def show_vakansy_form(request,form_attr):
	superuser_email = User.objects.get(pk=1,is_superuser=1).email
	if request.method == 'POST':
		if form_attr == 'repetitor':
			form = RepetitorForm(request.POST)
			which_form = 'repetitor'
			anketa_name = u'анкета репетитора'
		if form_attr == 'translate':
			form = TranslateForm(request.POST)
			which_form = 'translate'
			anketa_name = u'анкета переводчика'
		if form_attr == 'author':
			form = AuthorForm(request.POST)
			which_form = 'author'
			anketa_name = u'анкета автора'
		if form_attr == 'manager':
			form = ManagerForm(request.POST)
			which_form = 'manager'
			anketa_name = u'анкета менеджера'
		if form.is_valid():
			name = form.cleaned_data['name']
			new_author = form.save(commit=False)
			new_author.save()
			form.save_m2m()
			body =  u'На сайте поступил новый заказ на вакансию \n'
			body += 'http://'+domain+'/admin/indexpage/vakansy/' + unicode(new_author.pk)
			send_mail(u'На сайте заполнена: ' + anketa_name,body, 'from@studacademia.ru',[superuser_email], fail_silently=False)
			return render_to_response("vakansy.html",{'thanks':True}, context_instance=template.RequestContext(request))
	else:
		if form_attr == 'repetitor':
			form = RepetitorForm()
			which_form = 'repetitor'
		if form_attr == 'translate':
			form = TranslateForm()
			which_form = 'translate'
		if form_attr == 'author':
			form = AuthorForm()
			which_form = 'author'
		if form_attr == 'manager':
			form = ManagerForm()
			which_form = 'manager'
	return render_to_response("vakansy.html",{'form':form,'which_form':which_form,},context_instance=template.RequestContext(request))