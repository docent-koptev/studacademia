# -*- coding: utf-8 -*-
from rollyourown import seo

class SEOMetadata(seo.Metadata):
    title       = seo.Tag(head=True, max_length=250)
    description = seo.MetaTag(max_length=250)
    keywords    = seo.KeywordTag()
    class Meta:
   		seo_models = ('indexpage.Uslugi', 'indexpage.UpgradeUnic', 'indexpage.ElectroDocument', )
   		seo_views = ('show_index_page',)
