# -*- coding: utf-8 -*-
from django import forms
from django.forms import ModelForm
from models import UpgradeUnic,ElectroDocument,TranslateBlank,StudWork,Vakansy,Translate,Author,Manager
from chooses import *
from django.utils.translation import ugettext_lazy as _

#Повышение уникальности
class UpgradeUnicForm(ModelForm):
	class Meta:
		model = UpgradeUnic
		widgets = {
        	'name' : forms.TextInput(attrs = {'placeholder': 'необязательно'}),
        	'email'    :forms.TextInput(attrs = {'placeholder': 'необязательно'}),
        	'original_start'    :forms.TextInput(attrs = {'placeholder': 'необязательно'}),
        	'comment'    :forms.TextInput(attrs = {'placeholder': 'необязательно'}),
		}

#Работа с электронными документами
class ElectroDocumentForm(ModelForm):
	class Meta:
		model = ElectroDocument
		widgets = {
        	'name' : forms.TextInput(attrs = {'placeholder': 'необязательно'}),
        	'email'    :forms.TextInput(attrs = {'placeholder': 'необязательно'}),
        	'comment'    :forms.TextInput(attrs = {'placeholder': 'необязательно'}),
		}

#Бланк для перевода
class TranslateBlankForm(ModelForm):
	class Meta:
		model = TranslateBlank
		widgets = {
        	'name' : forms.TextInput(attrs = {'placeholder': 'необязательно'}),
        	'email'    :forms.TextInput(attrs = {'placeholder': 'необязательно'}),
        	'comment'    :forms.TextInput(attrs = {'placeholder': 'необязательно'}),
		}

#Учебные работы
class StudWorkForm(ModelForm):
	class Meta:
		model = StudWork
		widgets = {
        	'name' : forms.TextInput(attrs = {'placeholder': 'необязательно'}),
        	'email'    :forms.TextInput(attrs = {'placeholder': 'необязательно'}),
        	'comment'    :forms.TextInput(attrs = {'placeholder': 'необязательно'}),
        	'university'    :forms.TextInput(attrs = {'placeholder': 'необязательно'}),
        	'item'    :forms.TextInput(attrs = {'placeholder': 'необязательно'}),
        	'level'    :forms.TextInput(attrs = {'placeholder': 'необязательно'}),
        	'page'    :forms.TextInput(attrs = {'placeholder': 'необязательно'}),
        	'level'    :forms.TextInput(attrs = {'placeholder': 'необязательно'}),
        	'date'    :forms.TextInput(attrs = {'placeholder': 'необязательно'}),
		}

class RepetitorForm(ModelForm):
        class Meta:
                model = Vakansy
                fields = ('name','surname','father','city','phone','icq','skype','email','vuz','fac','year_finish','status','kvalification','stepen_study','zvanie_study','stage','subjects','doschool','first_cl','five_cl','nine_cl','ten_cl','vuz_cl','olymp_cl','ege_cl','universal_cl','podgotovka_cl','zero_ur','litle_ur','middle_ur','high_ur','mesto_this','mesto_uchenic','time_price','price_sposob','dop_info','obrabotka_confirm','how_known',)


class TranslateForm(ModelForm):
        class Meta:
                model = Translate
                fields = ('name','surname','father','city','phone','icq','skype','email','vuz','fac','year_finish','status','stage','opit','opit_in_company','dop_info','knew_pc','whick_lang','na_ino','in_ino','pismeni','posledov','sinhron','objom','finansi','urisprud','in_tehnolog','mexanika','mettalurg','medicina','sel_hoz','stroitelstvo','sudostroy','himija','aviacia','energetika','gaz_neft','geologija','hud_literatura','pishev_prom','electronika','fizika','ustn_per','pis_perevod','ustn_per_2','pis_perevod_2','na_ino_lang','na_rogn_lang','objom_translate_day','price_sposob','agree_na_obrabotku','how_known',)

class AuthorForm(ModelForm):
        class Meta:
                model = Author
                fields = ('name','surname','father','city','phone1','phone2','icq','skype','magent','email','vuz','fac','year_finish','kvalification','stepen_study','zvanie_study','price_sposob','number_chet','biznes_plan','diagram_table','diplom_work_project','dissertacia','dissertacia_candidat','magister_dissert','doklad_ok','comp_nabor','konspekti','kontrol_work','kurs_rabota','nauch_trud','otveti_na_bil','otchet_po_praktike','perevodi','repetitor','referat','recenzii','rech_k_diplomu','slide_na_baze','tezic_plan','chast_diploma','cherteg_program','ot_ruki','esse','have_language','design','gurnalistika','iskusstvo','istorija','kultura','literature','megd_otnosh','pedagogika','politologija','pshigologija','pravo','pr_reklama','sociologija','stranovedenie','filosophija','lang_znanie','arheologija','astronomija','biologija','geographija','estestvozn','medic','himija_ok','ekolog','vishka','materialovedenie','mashinostr','informatika','mehanika','programirovanie','process_apparat','stroika','transport_ok','fisika_ok','electronika_elektr','antikrizic','bankovsk_delo','buhuchet_audit','ved','gosupravlenie','del_etiket','marketings','megd_rinki','menedgment','micro_macro','nalogi','standartiz','statistika','strahovanie','upravlenie_persona','finansi_ok','how_known',)


class ManagerForm(ModelForm):
        class Meta:
                model = Manager
                fields = ('name','surname','father','city','phone1','phone2','icq','skype','email','vuz','fac','year_finish','status','stepen_neobhod','price_sposob', 'how_known',)