# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Vakansy'
        db.create_table(u'indexpage_vakansy', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('surname', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('father', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('icq', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('skype', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=100)),
            ('vuz', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('fac', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('year_finish', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('kvalification', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('stepen_study', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('zvanie_study', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('stage', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('first_cl', self.gf('django.db.models.fields.BooleanField')()),
            ('five_cl', self.gf('django.db.models.fields.BooleanField')()),
            ('nine_cl', self.gf('django.db.models.fields.BooleanField')()),
            ('ten_cl', self.gf('django.db.models.fields.BooleanField')()),
            ('vuz_cl', self.gf('django.db.models.fields.BooleanField')()),
            ('olymp_cl', self.gf('django.db.models.fields.BooleanField')()),
            ('ege_cl', self.gf('django.db.models.fields.BooleanField')()),
            ('universal_cl', self.gf('django.db.models.fields.BooleanField')()),
            ('podgotovka_cl', self.gf('django.db.models.fields.BooleanField')()),
            ('zero_ur', self.gf('django.db.models.fields.BooleanField')()),
            ('litle_ur', self.gf('django.db.models.fields.BooleanField')()),
            ('middle_ur', self.gf('django.db.models.fields.BooleanField')()),
            ('high_ur', self.gf('django.db.models.fields.BooleanField')()),
            ('time_obr', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('time_price', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('price_sposob', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('subjects', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('obrabotka_confirm', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('how_known', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
        ))
        db.send_create_signal(u'indexpage', ['Vakansy'])

        # Adding model 'Node'
        db.create_table(u'indexpage_node', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('vname', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('MText', self.gf('django.db.models.fields.TextField')()),
            ('PubDate', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'indexpage', ['Node'])


        # Changing field 'StudWork.note'
        db.alter_column(u'indexpage_studwork', 'note', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'ElectroDocument.work'
        db.alter_column(u'indexpage_electrodocument', 'work', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

    def backwards(self, orm):
        # Deleting model 'Vakansy'
        db.delete_table(u'indexpage_vakansy')

        # Deleting model 'Node'
        db.delete_table(u'indexpage_node')


        # User chose to not deal with backwards NULL issues for 'StudWork.note'
        raise RuntimeError("Cannot reverse this migration. 'StudWork.note' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'StudWork.note'
        db.alter_column(u'indexpage_studwork', 'note', self.gf('django.db.models.fields.CharField')(max_length=100))

        # User chose to not deal with backwards NULL issues for 'ElectroDocument.work'
        raise RuntimeError("Cannot reverse this migration. 'ElectroDocument.work' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'ElectroDocument.work'
        db.alter_column(u'indexpage_electrodocument', 'work', self.gf('django.db.models.fields.CharField')(max_length=100))

    models = {
        u'indexpage.electrodocument': {
            'Meta': {'object_name': 'ElectroDocument'},
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'document': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'work': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'indexpage.menubottom': {
            'Meta': {'object_name': 'Menubottom'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['indexpage.Parent']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'indexpage.node': {
            'MText': ('django.db.models.fields.TextField', [], {}),
            'Meta': {'object_name': 'Node'},
            'PubDate': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'vname': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        u'indexpage.parent': {
            'Meta': {'object_name': 'Parent'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'indexpage.slider': {
            'Meta': {'ordering': "('-position', 'show_on')", 'object_name': 'Slider'},
            'display': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'position': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'show_on': ('django.db.models.fields.CharField', [], {'default': "u'main'", 'max_length': '250'}),
            'target': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '10000', 'blank': 'True'})
        },
        u'indexpage.studwork': {
            'Meta': {'object_name': 'StudWork'},
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'document': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'level': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'note': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'page': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'topic': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'type_work': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['indexpage.Work']"}),
            'university': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'indexpage.translateblank': {
            'Meta': {'object_name': 'TranslateBlank'},
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'document': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'finish_language': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'original_language': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'indexpage.upgradeunic': {
            'Meta': {'object_name': 'UpgradeUnic'},
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'document': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'original_finish': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'original_start': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'sposob': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'indexpage.uslugi': {
            'Meta': {'object_name': 'Uslugi'},
            'background_class': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'day': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'form_name': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['indexpage.Menubottom']", 'null': 'True', 'blank': 'True'}),
            'price': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'indexpage.vakansy': {
            'Meta': {'object_name': 'Vakansy'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'ege_cl': ('django.db.models.fields.BooleanField', [], {}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '100'}),
            'fac': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'father': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'first_cl': ('django.db.models.fields.BooleanField', [], {}),
            'five_cl': ('django.db.models.fields.BooleanField', [], {}),
            'high_ur': ('django.db.models.fields.BooleanField', [], {}),
            'how_known': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'icq': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'kvalification': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'litle_ur': ('django.db.models.fields.BooleanField', [], {}),
            'middle_ur': ('django.db.models.fields.BooleanField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'nine_cl': ('django.db.models.fields.BooleanField', [], {}),
            'obrabotka_confirm': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'olymp_cl': ('django.db.models.fields.BooleanField', [], {}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'podgotovka_cl': ('django.db.models.fields.BooleanField', [], {}),
            'price_sposob': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'skype': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'stage': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'stepen_study': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'subjects': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'surname': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'ten_cl': ('django.db.models.fields.BooleanField', [], {}),
            'time_obr': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'time_price': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'universal_cl': ('django.db.models.fields.BooleanField', [], {}),
            'vuz': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'vuz_cl': ('django.db.models.fields.BooleanField', [], {}),
            'year_finish': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'zero_ur': ('django.db.models.fields.BooleanField', [], {}),
            'zvanie_study': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'indexpage.work': {
            'Meta': {'object_name': 'Work'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['indexpage']