# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'Uslugi'
        db.create_table(u'indexpage_uslugi', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('background_class', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('price', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('day', self.gf('django.db.models.fields.CharField')(max_length=10)),
        ))
        db.send_create_signal(u'indexpage', ['Uslugi'])

        # Adding model 'Parent'
        db.create_table(u'indexpage_parent', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'indexpage', ['Parent'])

        # Adding model 'Menubottom'
        db.create_table(u'indexpage_menubottom', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['indexpage.Parent'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50, db_index=True)),
            ('text', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'indexpage', ['Menubottom'])


    def backwards(self, orm):
        
        # Deleting model 'Uslugi'
        db.delete_table(u'indexpage_uslugi')

        # Deleting model 'Parent'
        db.delete_table(u'indexpage_parent')

        # Deleting model 'Menubottom'
        db.delete_table(u'indexpage_menubottom')


    models = {
        u'indexpage.menubottom': {
            'Meta': {'object_name': 'Menubottom'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['indexpage.Parent']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'db_index': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'indexpage.parent': {
            'Meta': {'object_name': 'Parent'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'indexpage.uslugi': {
            'Meta': {'object_name': 'Uslugi'},
            'background_class': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'day': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        }
    }

    complete_apps = ['indexpage']
