# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Translate.na_ino_lang'
        db.alter_column(u'indexpage_translate', 'na_ino_lang', self.gf('django.db.models.fields.BooleanField')(max_length=100))

        # Changing field 'Translate.na_rogn_lang'
        db.alter_column(u'indexpage_translate', 'na_rogn_lang', self.gf('django.db.models.fields.BooleanField')(max_length=100))

    def backwards(self, orm):

        # Changing field 'Translate.na_ino_lang'
        db.alter_column(u'indexpage_translate', 'na_ino_lang', self.gf('django.db.models.fields.CharField')(max_length=100))

        # Changing field 'Translate.na_rogn_lang'
        db.alter_column(u'indexpage_translate', 'na_rogn_lang', self.gf('django.db.models.fields.CharField')(max_length=100))

    models = {
        u'indexpage.electrodocument': {
            'Meta': {'object_name': 'ElectroDocument'},
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'document': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'work': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'indexpage.menubottom': {
            'Meta': {'object_name': 'Menubottom'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['indexpage.Parent']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'indexpage.node': {
            'MText': ('django.db.models.fields.TextField', [], {}),
            'Meta': {'object_name': 'Node'},
            'PubDate': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'vname': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        u'indexpage.parent': {
            'Meta': {'object_name': 'Parent'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'indexpage.slider': {
            'Meta': {'ordering': "('-position', 'show_on')", 'object_name': 'Slider'},
            'display': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'position': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'show_on': ('django.db.models.fields.CharField', [], {'default': "u'main'", 'max_length': '250'}),
            'target': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '10000', 'blank': 'True'})
        },
        u'indexpage.studwork': {
            'Meta': {'object_name': 'StudWork'},
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'document': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'level': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'note': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'page': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'topic': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'type_work': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['indexpage.Work']"}),
            'university': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'indexpage.translate': {
            'Meta': {'object_name': 'Translate'},
            'agree_na_obrabotku': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'aviacia': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'dop_info': ('django.db.models.fields.TextField', [], {}),
            'electronika': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '100'}),
            'energetika': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'fac': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'father': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'finansi': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'fizika': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'gaz_neft': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'geologija': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'himija': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'how_known': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'hud_literatura': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'icq': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_ino': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'in_tehnolog': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'knew_pc': ('django.db.models.fields.TextField', [], {}),
            'medicina': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'mettalurg': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'mexanika': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'na_ino': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'na_ino_lang': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'max_length': '100'}),
            'na_rogn_lang': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'objom': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'objom_translate_day': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'opit': ('django.db.models.fields.TextField', [], {}),
            'opit_in_company': ('django.db.models.fields.TextField', [], {}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'pis_perevod': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'pis_perevod_2': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'pishev_prom': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'pismeni': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'posledov': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'price_sposob': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'sel_hoz': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'sinhron': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'skype': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'stage': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'stroitelstvo': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'sudostroy': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'surname': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'urisprud': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ustn_per': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'ustn_per_2': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'vuz': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'whick_lang': ('django.db.models.fields.TextField', [], {}),
            'year_finish': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'indexpage.translateblank': {
            'Meta': {'object_name': 'TranslateBlank'},
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'document': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'finish_language': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'original_language': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'indexpage.upgradeunic': {
            'Meta': {'object_name': 'UpgradeUnic'},
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'document': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'original_finish': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'original_start': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'sposob': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'indexpage.uslugi': {
            'Meta': {'object_name': 'Uslugi'},
            'background_class': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'day': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'form_name': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['indexpage.Menubottom']", 'null': 'True', 'blank': 'True'}),
            'price': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'indexpage.vakansy': {
            'Meta': {'object_name': 'Vakansy'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'dop_info': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'doschool': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ege_cl': ('django.db.models.fields.BooleanField', [], {}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '100'}),
            'fac': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'father': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'first_cl': ('django.db.models.fields.BooleanField', [], {}),
            'five_cl': ('django.db.models.fields.BooleanField', [], {}),
            'high_ur': ('django.db.models.fields.BooleanField', [], {}),
            'how_known': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'icq': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'kvalification': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'litle_ur': ('django.db.models.fields.BooleanField', [], {}),
            'mesto_this': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'mesto_uchenic': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'middle_ur': ('django.db.models.fields.BooleanField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'nine_cl': ('django.db.models.fields.BooleanField', [], {}),
            'obrabotka_confirm': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'olymp_cl': ('django.db.models.fields.BooleanField', [], {}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'podgotovka_cl': ('django.db.models.fields.BooleanField', [], {}),
            'price_sposob': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'skype': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'stage': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'stepen_study': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'subjects': ('django.db.models.fields.TextField', [], {}),
            'surname': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'ten_cl': ('django.db.models.fields.BooleanField', [], {}),
            'time_obr': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'time_price': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'universal_cl': ('django.db.models.fields.BooleanField', [], {}),
            'vuz': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'vuz_cl': ('django.db.models.fields.BooleanField', [], {}),
            'year_finish': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'zero_ur': ('django.db.models.fields.BooleanField', [], {}),
            'zvanie_study': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'indexpage.work': {
            'Meta': {'object_name': 'Work'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['indexpage']