# -*- coding: utf-8 -*-
from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.flatpages.models import FlatPage
from tinymce import models as tinymce_model
from sorl.thumbnail.fields import ImageField
from django.utils.translation import ugettext_lazy as _
from chooses import *

class Parent(models.Model):
	name = models.CharField(max_length=100,verbose_name=u'Название')
	def __unicode__(self):
		return unicode(self.name)
	class Meta:
		verbose_name = u'Категория'
		verbose_name_plural = u'Категория'


class Menubottom(models.Model):
	name = models.ForeignKey(Parent)
	title = models.CharField(max_length=100,verbose_name=u'Название')
	slug = models.SlugField(verbose_name=u'Красивый адрес')
	text = models.TextField(verbose_name=u'Текст')

	def __unicode__(self):
		return unicode(self.title)

	class Meta:
		verbose_name = u'Нижнее меню'
		verbose_name_plural = u'Нижнее меню'

	def get_absolute_url(self):
		return "/page/%i/" % self.slug


class Uslugi(models.Model):
	none = 'none'
	studwork = 'studwork'
	upgradeunic = 'upgradeunic'
	electro = 'electro'
	blank = 'blank'
	FORMA = (
		(none, 'Без ссылки'),
        (studwork, 'Учебные работы'),
        (upgradeunic, 'Повышение уникалльности'),
        (electro, 'Работа с электронными документами'),
        (blank, 'Бланк перевода'),
    )
	title = models.CharField(max_length=30,verbose_name=u'Заголовок')
	background_class = models.CharField(max_length=2,verbose_name=u'Фоновый цвет (b1,b2..)')
	price = models.CharField(max_length=30,verbose_name=u'Цена')
	day = models.CharField(max_length=10,verbose_name=u'Срок исполнения')
	form_name = models.CharField(max_length=300,choices = FORMA,verbose_name=u'Выводимая форма',null=True,blank=True)
	link = models.ForeignKey(Menubottom,verbose_name=u'Связанная страница',null=True,blank=True)
	def __unicode__(self):
		return u'%s %s' % (self.title, self.background_class)

	class Meta:
		verbose_name = u'Услуга'
		verbose_name_plural = u'Услуги'

	def get_absolute_url(self):
		return reverse('show_form', args=[str(self.form_name)])



class UpgradeUnic(models.Model):
	hand = 'hand'
	auto = 'auto'
	SPOSOB_OBRABOTKI = (
        (hand, 'Ручной(рерайтинг)'),
        (auto, 'Механический'),
    )
	name = models.CharField(max_length=100,verbose_name=u'Имя',null=True,blank=True)
	phone = models.CharField(max_length=100,verbose_name=u'Телефон')
	email = models.CharField(max_length=100,verbose_name=u'Еmail',null=True,blank=True)
	sposob = models.CharField(max_length=100,verbose_name=u'Обработка текста',choices=SPOSOB_OBRABOTKI)
	original_start = models.CharField(max_length=100,verbose_name=u'Оригинальность текущая',null=True,blank=True)
	original_finish = models.CharField(max_length=100,verbose_name=u'Желаемая оригинальность')
	date = models.CharField(max_length=100,verbose_name=u'Дата получения')
	comment = models.CharField(max_length=100,verbose_name=u'Комментарий',null=True,blank=True)
	document = models.FileField(upload_to='files',verbose_name=u'Файл',null=True,blank=True)

	def __unicode__(self):
		return unicode(self.name)

	class Meta:
		verbose_name = u'Повышение уникалльности'
		verbose_name_plural = u'Повышение уникалльности'
	
	def get_absolute_url(self):
		return "/upgradeunic/%d"  % self.pk

class ElectroDocument(models.Model):
	printtext = 'printtext'
	handtext = 'handtext'
	edit = 'edit'
	TYPE_WORK = (
        (printtext, 'Cо сканированного печатного текста'),
        (handtext, 'со сканированного рукописного текста'),
        (edit, 'Редактирование/Корректура'),
    )
	name = models.CharField(max_length=100,verbose_name=u'Имя',null=True,blank=True)
	phone = models.CharField(max_length=100,verbose_name=u'Телефон')
	email = models.CharField(max_length=100,verbose_name=u'Еmail',null=True,blank=True)
	work = models.CharField(max_length=100,verbose_name=u'Тип работы',choices=TYPE_WORK,null=True,blank=True)
	date = models.CharField(max_length=100,verbose_name=u'Дата получения')
	comment = models.CharField(max_length=100,verbose_name=u'Комментарий',null=True,blank=True)
	document = models.FileField(upload_to='files',verbose_name=u'Файл',null=True,blank=True)

	def __unicode__(self):
		return unicode(self.name)

	class Meta:
		verbose_name = u'Работа с электронными текстами'
		verbose_name_plural = u'Работа с электронными текстами'

	def get_absolute_url(self):
		return "/electrodocument/%d"  % self.pk

class TranslateBlank(models.Model):
	name = models.CharField(max_length=100,verbose_name=u'Имя',null=True,blank=True)
	phone = models.CharField(max_length=100,verbose_name=u'Телефон')
	email = models.CharField(max_length=100,verbose_name=u'Еmail',null=True,blank=True)
	original_language = models.CharField(max_length=100,verbose_name=u'Язык исходного текста')
	finish_language = models.CharField(max_length=100,verbose_name=u'Перевести на')
	date = models.CharField(max_length=100,verbose_name=u'Дата получения')
	comment = models.CharField(max_length=100,verbose_name=u'Комментарий',null=True,blank=True)
	document = models.FileField(upload_to='files',verbose_name=u'Файл',null=True,blank=True)

	def __unicode__(self):
		return unicode(self.name)

	class Meta:
		verbose_name = u'Бланк для переводов'
		verbose_name_plural = u'Бланк для переводов'

	def get_absolute_url(self):
		return "/translateblank/%d"  % self.pk


class Work(models.Model):
	name = models.CharField(max_length=100,verbose_name=u'Название')

	def __unicode__(self):
		return unicode(self.name)

	class Meta:
		verbose_name = u'Виды работ'
		verbose_name_plural = u'Виды работ'


class StudWork(models.Model):
	none = 'none'
	krugl = 'krugl'
	podstr = 'podstr'
	kvadtat = 'kvadtat'
	SNOSKI = (
        (none, 'Нет'),
        (krugl, 'Круглые скобки'),
        (podstr, 'Подстрочные'),
        (kvadtat, 'Квадратные'),
    )
	type_work = models.ForeignKey(Work,verbose_name=u'Тип работы')
	university = models.CharField(max_length=100,verbose_name=u'Университет',null=True,blank=True)
	topic = models.CharField(max_length=100,verbose_name=u'Тема')
	item = models.CharField(max_length=100,verbose_name=u'Название предмета',null=True,blank=True)
	date = models.CharField(max_length=100,verbose_name=u'Дата сдачи',null=True,blank=True)
	level = models.CharField(max_length=100,verbose_name=u'Уровень уникальности',null=True,blank=True)
	page = models.CharField(max_length=100,verbose_name=u'Количество страниц',null=True,blank=True)
	note = models.CharField(max_length=100,verbose_name=u'Наличие сносок',choices=SNOSKI,null=True,blank=True)
	name = models.CharField(max_length=100,verbose_name=u'Имя',null=True,blank=True)
	phone = models.CharField(max_length=100,verbose_name=u'Телефон')
	email = models.CharField(max_length=100,verbose_name=u'Еmail',null=True,blank=True)
	comment = models.CharField(max_length=100,verbose_name=u'Комментарий',null=True,blank=True)
	document = models.FileField(upload_to='files',verbose_name=u'Файл',null=True,blank=True)

	def __unicode__(self):
		return self.topic

	class Meta:
		verbose_name = u'Учебные работы'
		verbose_name_plural = u'Учебные работы'

	def get_absolute_url(self):
		return "/studwork/%d"  % self.pk


class Slider(models.Model):
    title = models.CharField(_(u'заголовок'), blank=True, max_length=250, help_text=_(u'Рабочее название для админки.'))
    show_on = models.CharField(_(u'показывать на'), default=u'main', max_length=250,
                               help_text=_(u'принадлежность к странице, на которой будет находиться слайд'),
                               choices=(("main", "главной странице (580×300)"),))
    image = ImageField(_(u'изображение'), upload_to='slider')
    name = models.CharField(_(u'название'), blank=True, max_length=250, help_text=_(u'будет показываться при наведении на слайд; максимум 250 символов.'))
    url = models.CharField(_(u'ссылка'), blank=True, max_length=10000)
    target = models.BooleanField(_(u"открывать ссылку в новом окне"), default=False)
    display = models.BooleanField(_(u'отображать'), default=True)
    position = models.IntegerField(_(u'позиция'), default=100, help_text=_(u'номер для сортировки слайдов'))

    class Meta:
        ordering = ("-position", "show_on", )
        verbose_name = u"слайд"
        verbose_name_plural = u'слайды'

    def __unicode__(self):
        return u'%s: %s' % ( self.show_on, self.title)



class Node(models.Model):
    vname = models.CharField(max_length=150)
    MText = models.TextField()
    PubDate = models.DateTimeField()
 
    class Meta:
        verbose_name = 'Node'
        verbose_name_plural = 'Nodes'
 
    def __unicode__(self):
        return self.vname


#Vakansy model for vakansy form
class Vakansy(models.Model):
        #Общая инфа
        name = models.CharField(verbose_name=_(u"Имя"),max_length=100)
        surname = models.CharField(verbose_name=_(u"Фамилия"),max_length=100)
        father = models.CharField(verbose_name=_(u"Отчеcтво"),max_length=100)

        #Где живет
        city = models.CharField(verbose_name=_(u"Город проживания"),max_length=100)
        phone = models.CharField(verbose_name=_(u"Телефон"),max_length=100)
        icq = models.CharField(verbose_name=_(u"ICQ"),max_length=100,blank=True)
        skype = models.CharField(verbose_name=_(u"SKYPE"),max_length=100,blank=True)
        email = models.EmailField(verbose_name=_(u"E-mail"),max_length=100)

        #Образование
        vuz = models.CharField(verbose_name=_(u"Название ВУЗа"),max_length=100)
        fac = models.CharField(verbose_name=_(u"Факультет"),max_length=100,blank=True,help_text=u"suka")
        year_finish = models.CharField(verbose_name=_(u"Год окончания"),max_length=100)
        status = models.CharField(verbose_name=_(u"Статус"),choices=STATUS,max_length=100)
        kvalification = models.CharField(verbose_name=_(u"Уровень квалификации"),choices=CATEGORY,max_length=100)
        stepen_study = models.CharField(verbose_name=_(u"Ученая степень"),choices=STEPEN,max_length=100)
        zvanie_study = models.CharField(verbose_name=_(u"Ученое звание"),choices=STUDY_ZVANIE,max_length=100)
        stage = models.CharField(verbose_name=_(u"лет"),max_length=100)
        subjects = models.TextField(verbose_name=_(u"Преподаваемые Вами предметы"))
        doschool = models.BooleanField(verbose_name=_(u"дошкольное образование"),default=False)
        first_cl = models.BooleanField(verbose_name=_(u"1-4 классы"),blank=True)
        five_cl = models.BooleanField(verbose_name=_(u"5-8 классы"),blank=True)
        nine_cl = models.BooleanField(verbose_name=_(u"9 классы, ГИА"),blank=True)
        ten_cl = models.BooleanField(verbose_name=_(u"10-11 классы"),blank=True)
        vuz_cl = models.BooleanField(verbose_name=_(u"для студентов ВУЗов"),blank=True)
        olymp_cl = models.BooleanField(verbose_name=_(u"подготовка к олимпиаде"),blank=True)
        ege_cl = models.BooleanField(verbose_name=_(u"подготовка к ЕГЭ 10 -11 классы"),blank=True)
        universal_cl = models.BooleanField(verbose_name=_(u"сдача международных экзаменов"),blank=True)
        podgotovka_cl = models.BooleanField(verbose_name=_(u"подготовка в ВУЗы, в которых проводятся дополнительные экзамены"),blank=True)

        zero_ur = models.BooleanField(verbose_name=_(u"обучение с «нуля»"),blank=True)
        litle_ur = models.BooleanField(verbose_name=_(u"слабый уровень знаний"),blank=True)
        middle_ur = models.BooleanField(verbose_name=_(u"средний уровень знаний"),blank=True)
        high_ur = models.BooleanField(verbose_name=_(u"повышенный уровень знаний"),blank=True)

        #Место проведения занятий:
        mesto_this = models.BooleanField(verbose_name=_(u"на территории репетитора"),default=False)
        mesto_uchenic = models.BooleanField(verbose_name=_(u"на территории ученика"),default=False)

        #Время обращения
        time_obr = models.CharField(verbose_name=_(u"Время обращения"),choices=TIME,max_length=100)

        #Стоимость часа
        time_price = models.CharField(verbose_name=_(u"Стоимость часа"),max_length=100)

        #Способ оплаты
        price_sposob = models.CharField(verbose_name=_(u"Удобный для Вас способ оплаты:"),choices=PRICE_SPOSOB,max_length=100)

        #Доп.инфа
        dop_info = models.TextField(verbose_name=_(u"Дополнительная информация (о себе, о методиках преподавания и т.д.)"),blank=True)

        #Согласен ли на обработку данных
        obrabotka_confirm = models.CharField(verbose_name=_(u"Согласен на обработку моих персональных данных"),choices=YESNO,max_length=100)

        #Как узнали
        how_known = models.CharField(verbose_name=_(u"Как Вы узнали о нас"),max_length=100,blank=True)

        def __unicode__(self):
        	return unicode(self.name)

        class Meta:
        	verbose_name=u"Репетиторов"
        	verbose_name_plural=u"Репетиторов"

#Class for translate model (extends Vakansy)
class Translate(models.Model):
	#Общая инфа
	name = models.CharField(verbose_name=_(u"Имя"),max_length=100)
	surname = models.CharField(verbose_name=_(u"Фамилия"),max_length=100)
	father = models.CharField(verbose_name=_(u"Отчеcтво"),max_length=100)
	#Где живет
	city = models.CharField(verbose_name=_(u"Город проживания"),max_length=100)
	phone = models.CharField(verbose_name=_(u"Телефон"),max_length=100)
	icq = models.CharField(verbose_name=_(u"ICQ"),max_length=100,blank=True)
	skype = models.CharField(verbose_name=_(u"SKYPE"),max_length=100,blank=True)
	email = models.EmailField(verbose_name=_(u"E-mail"),max_length=100)

	#Образование
	vuz = models.CharField(verbose_name=_(u"Название ВУЗа"),max_length=100)
	fac = models.CharField(verbose_name=_(u"Факультет"),max_length=100,blank=True,help_text=u"")
	year_finish = models.CharField(verbose_name=_(u"Год окончания"),max_length=100)
	status = models.CharField(verbose_name=_(u"Статус"),choices=STATUS,max_length=100)

	stage = models.CharField(verbose_name=_(u"лет"),max_length=100)
	opit = models.TextField(verbose_name=_(u"Опишите Ваш профессиональный опыт (переводческий)"))
	opit_in_company = models.TextField(verbose_name=_(u"Опыт работы в переводческих компаниях"))
	dop_info = models.TextField(verbose_name=_(u"Дополнительная информация"))
	knew_pc = models.TextField(verbose_name=_(u"Знание компьютерных программ"))
	whick_lang = models.TextField(verbose_name=_(u"Какими иностранными языками Вы профессионально владеете"))

	na_ino = models.BooleanField(verbose_name=_(u"на иностранный язык"),default=False)
	in_ino = models.BooleanField(verbose_name=_(u"с иностранного языка"),default=False)
	pismeni = models.BooleanField(verbose_name=_(u"письменный"),default=False)
	posledov = models.BooleanField(verbose_name=_(u"последовательный"),default=False)
	sinhron = models.BooleanField(verbose_name=_(u"синхронный"),default=False)

	#Объем в день
	objom = models.CharField(verbose_name=_(u"Максимальный объем в день"),choices=MAX_VEIGHT,max_length=100)

	#Предпочитаемые тематики переводов:
	finansi = models.BooleanField(verbose_name=_(u"финансы, бухгалтерия, экономика"),default=False)
	urisprud = models.BooleanField(verbose_name=_(u"юриспруденция"),default=False)
	in_tehnolog = models.BooleanField(verbose_name=_(u"информационные технологии "),default=False)
	mexanika = models.BooleanField(verbose_name=_(u"машиностроение, механика"),default=False)
	mettalurg = models.BooleanField(verbose_name=_(u"металлургия"),default=False)
	medicina = models.BooleanField(verbose_name=_(u"медицина "),default=False)
	sel_hoz = models.BooleanField(verbose_name=_(u"сельское хозяйство"),default=False)
	stroitelstvo = models.BooleanField(verbose_name=_(u"строительство"),default=False)
	sudostroy = models.BooleanField(verbose_name=_(u"судостроение"),default=False)
	himija = models.BooleanField(verbose_name=_(u"химия, биология, косметика"),default=False)
	aviacia = models.BooleanField(verbose_name=_(u"авиация, космическая техника "),default=False)
	energetika = models.BooleanField(verbose_name=_(u"энергетика"),default=False)
	gaz_neft = models.BooleanField(verbose_name=_(u"нефть, газ "),default=False)
	geologija = models.BooleanField(verbose_name=_(u"геология"),default=False)
	hud_literatura = models.BooleanField(verbose_name=_(u"художественная литература"),default=False)
	pishev_prom = models.BooleanField(verbose_name=_(u"пищевая промышленность"),default=False)
	electronika = models.BooleanField(verbose_name=_(u"электроника "),default=False)
	fizika = models.BooleanField(verbose_name=_(u"физика"),default=False)

	#Желаемая зарплата
	ustn_per = models.CharField(verbose_name=_(u"устные переводы за час"),max_length=100,blank=True,help_text=u"Желаемая зарплата")
	pis_perevod = models.CharField(verbose_name=_(u"письменные переводы (за 1800 зн.)"),max_length=100,blank=True,help_text=u"Желаемая зарплата")

	#Минимальная зарплата
	ustn_per_2 = models.CharField(verbose_name=_(u"устные переводы за час"),max_length=100,blank=True,help_text=u"Минимальная зарплата")
	pis_perevod_2 = models.CharField(verbose_name=_(u"письменные переводы (за 1800 зн.)"),max_length=100,blank=True,help_text=u"Минимальная зарплата")

	#Работа в качестве редактора переводимых текстов:
	na_ino_lang = models.BooleanField(verbose_name=_(u"на иностранный язык"),max_length=100,blank=True,help_text=u"Работа в качестве редактора переводимых текстов",default=False)
	na_rogn_lang = models.BooleanField(verbose_name=_(u"на родной язык"),max_length=100,blank=True,help_text=u"Работа в качестве редактора переводимых текстов:",default=False)

	#Объем перевода в день
	objom_translate_day = models.CharField(verbose_name=_(u"Максимальный объем редактирования в день"),choices=MAX_VEIGHT,max_length=100)

	#Способ оплаты
	price_sposob = models.CharField(verbose_name=_(u"Удобный для Вас способ оплаты"),choices=PRICE_SPOSOB,max_length=100)

	#Согласен на обработку данных
	agree_na_obrabotku = models.CharField(verbose_name=_(u"Согласен на обработку моих персональных данных"),choices=YESNO,max_length=100)

	#Как узнали
	how_known = models.CharField(verbose_name=_(u"Как Вы узнали о нас"),max_length=100,blank=True)

	def __unicode__(self):
		return unicode(self.name)

	class Meta:
		verbose_name=u"Переводчиков"
		verbose_name_plural=u"Переводчиков"



#Class for author model
class Author(models.Model):
	#Общая инфа
	name = models.CharField(verbose_name=_(u"Имя"),max_length=100)
	surname = models.CharField(verbose_name=_(u"Фамилия"),max_length=100)
	father = models.CharField(verbose_name=_(u"Отчеcтво"),max_length=100)

	#Где живет
	city = models.CharField(verbose_name=_(u"Город проживания"),max_length=100)
	phone1 = models.CharField(verbose_name=_(u"Основной телефон"),max_length=100)
	phone2 = models.CharField(verbose_name=_(u"Дополнительный телефон (если имеется)"),max_length=100,blank=True)
	icq = models.CharField(verbose_name=_(u"icq"),max_length=100,blank=True)
	skype = models.CharField(verbose_name=_(u"Skype"),max_length=100,blank=True)
	magent = models.CharField(verbose_name=_(u"MailAgent (если имеется)"),max_length=100,blank=True)
	email = models.EmailField(verbose_name=_(u"E-mail"),max_length=100)

	#Образование
	vuz = models.CharField(verbose_name=_(u"Название ВУЗа"),max_length=100)
	fac = models.CharField(verbose_name=_(u"Факультет"),max_length=100,blank=True,help_text=u"")
	year_finish = models.CharField(verbose_name=_(u"Год окончания"),max_length=100)
	status = models.CharField(verbose_name=_(u"Статус"),choices=STATUS,max_length=100)
	kvalification = models.CharField(verbose_name=_(u"Уровень квалификации"),choices=CATEGORY,max_length=100)
	stepen_study = models.CharField(verbose_name=_(u"Ученая степень"),choices=STEPEN,max_length=100)
	zvanie_study = models.CharField(verbose_name=_(u"Ученое звание"),choices=STUDY_ZVANIE,max_length=100)

	#Способ оплаты
	price_sposob = models.CharField(verbose_name=_(u"Удобный для Вас способ оплаты"),choices=PRICE_SPOSOB,max_length=100)
	number_chet = models.TextField(verbose_name=_(u"Номер кошелька/счета"),blank=True)

	biznes_plan = models.BooleanField(verbose_name=_(u"Бизнес-план"),default=False)
	diagram_table = models.BooleanField(verbose_name=_(u"Диаграммы, таблицы"),default=False)
	diplom_work_project = models.BooleanField(verbose_name=_(u"Дипломная работа/ дипломный проект"),default=False)
	dissertacia = models.BooleanField(verbose_name=_(u"Докторская диссертация"),default=False)
	dissertacia_candidat = models.BooleanField(verbose_name=_(u"Кандидатская диссертация"),default=False)
	magister_dissert = models.BooleanField(verbose_name=_(u"Магистерская диссертация"),default=False)
	doklad_ok = models.BooleanField(verbose_name=_(u"Доклад"),default=False)
	comp_nabor = models.BooleanField(verbose_name=_(u"Компьютерный набор текста"),default=False)
	konspekti = models.BooleanField(verbose_name=_(u"Конспекты"),default=False)
	kontrol_work = models.BooleanField(verbose_name=_(u"Контрольная работа"),default=False)
	kurs_rabota = models.BooleanField(verbose_name=_(u"Курсовая работа / курсовой проект"),default=False)
	nauch_trud = models.BooleanField(verbose_name=_(u"Научный труд / исследование"),default=False)
	otveti_na_bil = models.BooleanField(verbose_name=_(u"Ответы на билеты"),default=False)
	otchet_po_praktike = models.BooleanField(verbose_name=_(u"Отчет по практике / исследование"),default=False)
	perevodi = models.BooleanField(verbose_name=_(u"Переводы "),default=False)
	repetitor = models.BooleanField(verbose_name=_(u"Репетитор"),default=False)
	referat = models.BooleanField(verbose_name=_(u"Реферат "),default=False)
	recenzii = models.BooleanField(verbose_name=_(u"Рецензии "),default=False)
	rech_k_diplomu = models.BooleanField(verbose_name=_(u"Речь к диплому"),default=False)
	slide_na_baze = models.BooleanField(verbose_name=_(u"Слайды (на базе PowerPoint)"),default=False)
	tezic_plan = models.BooleanField(verbose_name=_(u"Тезисный план"),default=False)
	chast_diploma = models.BooleanField(verbose_name=_(u"Часть диплома"),default=False)
	cherteg_program = models.BooleanField(verbose_name=_(u"Чертежи в  программах"),default=False)
	ot_ruki = models.BooleanField(verbose_name=_(u"Чертежи от руки"),default=False)
	esse = models.BooleanField(verbose_name=_(u" Эссе "),default=False)
	have_language = models.TextField(verbose_name=_(u"Укажите иностранные языки, которыми Вы владеете"),blank=True)

	design = models.BooleanField(verbose_name=_(u"Дизайн"),default=False)
	gurnalistika = models.BooleanField(verbose_name=_(u"Журналистика"),default=False)
	iskusstvo = models.BooleanField(verbose_name=_(u"Искусство"),default=False)
	istorija = models.BooleanField(verbose_name=_(u"История"),default=False)
	kultura = models.BooleanField(verbose_name=_(u"Культура"),default=False)
	literature = models.BooleanField(verbose_name=_(u"Литература"),default=False)
	megd_otnosh = models.BooleanField(verbose_name=_(u"Международные отношения"),default=False)
	pedagogika = models.BooleanField(verbose_name=_(u"Педагогика"),default=False)
	politologija = models.BooleanField(verbose_name=_(u"Политология"),default=False)
	pshigologija = models.BooleanField(verbose_name=_(u"Психология"),default=False)
	pravo = models.BooleanField(verbose_name=_(u"Право"),default=False)
	pr_reklama = models.BooleanField(verbose_name=_(u"Реклама и PR"),default=False)
	sociologija = models.BooleanField(verbose_name=_(u"Социология"),default=False)
	stranovedenie = models.BooleanField(verbose_name=_(u"Страноведение"),default=False)
	filosophija = models.BooleanField(verbose_name=_(u"Философия"),default=False)
	lang_znanie = models.BooleanField(verbose_name=_(u"Языкознание"),default=False)
	arheologija = models.BooleanField(verbose_name=_(u"Археология"),default=False)
	astronomija = models.BooleanField(verbose_name=_(u"Астрономия"),default=False)
	biologija = models.BooleanField(verbose_name=_(u"Биология"),default=False)
	
	geographija = models.BooleanField(verbose_name=_(u"География"),default=False)
	estestvozn = models.BooleanField(verbose_name=_(u"Естествознание"),default=False)
	medic = models.BooleanField(verbose_name=_(u"Медицина"),default=False)
	himija_ok = models.BooleanField(verbose_name=_(u"Химия"),default=False)
	ekolog = models.BooleanField(verbose_name=_(u"Экология "),default=False)

	vishka = models.BooleanField(verbose_name=_(u"Высшая математика"),default=False)
	materialovedenie = models.BooleanField(verbose_name=_(u"Материаловедение"),default=False)
	mashinostr = models.BooleanField(verbose_name=_(u"Машиностроение"),default=False)
	informatika = models.BooleanField(verbose_name=_(u"Информатика "),default=False)
	mehanika = models.BooleanField(verbose_name=_(u"Механика"),default=False)
	programirovanie = models.BooleanField(verbose_name=_(u"Программирование "),default=False)
	process_apparat = models.BooleanField(verbose_name=_(u"Процессы и аппараты"),default=False)

	stroika = models.BooleanField(verbose_name=_(u"Строительство "),default=False)
	transport_ok = models.BooleanField(verbose_name=_(u"Транспорт"),default=False)
	fisika_ok = models.BooleanField(verbose_name=_(u"Физика"),default=False)
	electronika_elektr = models.BooleanField(verbose_name=_(u"Электроника, электротехника"),default=False)
	antikrizic = models.BooleanField(verbose_name=_(u"Антикризисное управление"),default=False)
	bankovsk_delo = models.BooleanField(verbose_name=_(u"Банковское дело"),default=False)
	buhuchet_audit = models.BooleanField(verbose_name=_(u"Бухучет и аудит"),default=False)

	ved = models.BooleanField(verbose_name=_(u"ВЭД"),default=False)
	gosupravlenie = models.BooleanField(verbose_name=_(u"Гос. и муниципальное управление"),default=False)
	del_etiket = models.BooleanField(verbose_name=_(u"Деловой этикет "),default=False)
	marketings = models.BooleanField(verbose_name=_(u"Маркетинг "),default=False)
	megd_rinki = models.BooleanField(verbose_name=_(u"Международные рынки"),default=False)
	menedgment = models.BooleanField(verbose_name=_(u"Менеджмент"),default=False)
	micro_macro = models.BooleanField(verbose_name=_(u"Микро-, макроэкономика"),default=False)

	nalogi = models.BooleanField(verbose_name=_(u"Налоги"),default=False)
	standartiz = models.BooleanField(verbose_name=_(u"Стандартизация"),default=False)
	statistika = models.BooleanField(verbose_name=_(u"Статистика "),default=False)
	strahovanie = models.BooleanField(verbose_name=_(u"Страхование"),default=False)
	upravlenie_persona = models.BooleanField(verbose_name=_(u"Управление персоналом"),default=False)
	finansi_ok = models.BooleanField(verbose_name=_(u"Финансы"),default=False)
	
	#Как узнали
	how_known = models.CharField(verbose_name=_(u"Как Вы узнали о нас"),max_length=100,blank=True)

	def __unicode__(self):
		return unicode(self.name)

	class Meta:
		verbose_name=u"Авторов"
		verbose_name_plural=u"Авторов"



class Manager(models.Model):
	#Общая инфа
	name = models.CharField(verbose_name=_(u"Имя"),max_length=100)
	surname = models.CharField(verbose_name=_(u"Фамилия"),max_length=100)
	father = models.CharField(verbose_name=_(u"Отчеcтво"),max_length=100)

	#Где живет
	city = models.CharField(verbose_name=_(u"Город проживания"),max_length=100)
	phone1 = models.CharField(verbose_name=_(u"Основной телефон"),max_length=100)
	phone2 = models.CharField(verbose_name=_(u"Дополнительный телефон (если имеется)"),max_length=100,blank=True)
	icq = models.CharField(verbose_name=_(u"icq"),max_length=100,blank=True)
	skype = models.CharField(verbose_name=_(u"Skype"),max_length=100,blank=True)
	magent = models.CharField(verbose_name=_(u"MailAgent (если имеется)"),max_length=100,blank=True)
	email = models.EmailField(verbose_name=_(u"E-mail"),max_length=100)

	#Образование
	vuz = models.CharField(verbose_name=_(u"Название ВУЗа"),max_length=100)
	fac = models.CharField(verbose_name=_(u"Факультет"),max_length=100,blank=True,help_text=u"")
	year_finish = models.CharField(verbose_name=_(u"Год окончания"),max_length=100)
	status = models.CharField(verbose_name=_(u"Статус"),choices=STATUS,max_length=100)

	stepen_neobhod = models.CharField(verbose_name=_(u"Степень необходимости денег"),choices=STEPEN_NEOBHOD_DENEG,max_length=100,null=True,blank=True)

	#Способ оплаты
	price_sposob = models.CharField(verbose_name=_(u"Удобный для Вас способ оплаты:"),choices=PRICE_SPOSOB,max_length=100)

	#Как узнали
	how_known = models.CharField(verbose_name=_(u"Как Вы узнали о нас"),max_length=100,blank=True)

	def __unicode__(self):
		return unicode(self.name)

	class Meta:
		verbose_name=u"Менеджеров"
		verbose_name_plural=u"Менеджеров"

