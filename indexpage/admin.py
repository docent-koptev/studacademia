from django.contrib import admin
from models import Manager,Uslugi,Menubottom,Parent,UpgradeUnic,ElectroDocument,TranslateBlank,Work,StudWork,Slider,Vakansy,Translate,Author
from django.contrib.flatpages.admin import FlatPageAdmin, FlatpageForm
from django.contrib.flatpages.models import FlatPage
#from rollyourown.seo.admin import register_seo_admin
#from seo import SEOMetadata
from django.utils.translation import gettext_lazy as _
from sorl.thumbnail.shortcuts import get_thumbnail

#register_seo_admin(admin.site, SEOMetadata)

class MyFlatPageAdmin(FlatPageAdmin):
    class Media:
		js = [
            '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
            '/static/grappelli/tinymce_setup/tinymce_setup.js',
		]

class UslugiAdmin(admin.ModelAdmin):
	list_display = ('id','title', 'price','day','form_name')

class OrderAdmin(admin.ModelAdmin):
	pass

class MenubottomAdmin(admin.ModelAdmin):
	list_display = ('id','name', 'title','slug','text')
	prepopulated_fields = {"slug": ("title",)}
	class Media:
		js = [
            '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
            '/static/grappelli/tinymce_setup/tinymce_setup.js',
		]

class ParentAdmin(admin.ModelAdmin):
	pass

class UpgradeUnicAdmin(admin.ModelAdmin):
	list_display = ('id','name', 'phone','email','comment','date','document')
	def has_add_permission(self, request):
		return False

class VakansyAdmin(admin.ModelAdmin):
	def has_add_permission(self, request):
		return False


admin.site.register(Uslugi,UslugiAdmin)
admin.site.register(Menubottom,MenubottomAdmin)
admin.site.register(Parent,ParentAdmin)

admin.site.register(UpgradeUnic,UpgradeUnicAdmin)
admin.site.register(ElectroDocument,UpgradeUnicAdmin)
admin.site.register(TranslateBlank,UpgradeUnicAdmin)
admin.site.register(Work)
admin.site.register(StudWork,UpgradeUnicAdmin)

admin.site.unregister(FlatPage)
admin.site.register(FlatPage, MyFlatPageAdmin)

admin.site.register(Slider)
admin.site.register(Vakansy,VakansyAdmin)
admin.site.register(Translate,VakansyAdmin)
admin.site.register(Author,VakansyAdmin)
admin.site.register(Manager,VakansyAdmin)