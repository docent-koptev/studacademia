# -*- coding: utf-8 -*-
from django.template import Library, Node
from django.contrib.flatpages.models import FlatPage
from indexpage.models import Menubottom,Parent
register = Library()

#template tag for create menu in a top (with link)
@register.simple_tag(takes_context = True)
def flatpage_menu(context):
    pages = FlatPage.objects.all().order_by('id')
    menu = '<ul>'
    request = context['request']
    active = ''
    for i in range(len(pages)):
    	if request.path == pages[i].url:
    		active = 'active'
    	else:
    		active = ''
        menu += '<li>'+'<a class="'+active+'" href="'+pages[i].url+'" title="'+pages[i].title+'">'+pages[i].title+'</a></li>'
    menu += '</ul>'
    return menu 


#template tag for create menu in a bottom
@register.simple_tag(takes_context = True)
def bottom_menu(context):
    menu_block = ''
    parent_element = Parent.objects.all()
    request = context['request']
    for i in range(len(parent_element)):
        child_element = Menubottom.objects.filter(name=parent_element[i].pk)
        if i==0:
            menu = '<div class="col1">'
        else:
            menu = '<div class="col2">'
        menu +='<h3>'+parent_element[i].name+'</h3>'
        menu += '<ul>'
        for j in range(len(child_element)):
            if request.path == '/page/'+child_element[j].slug+'/':
                active = 'color'
            else:
                active = ''
            menu +='<li><a class="'+active+'" href="/page/'+child_element[j].slug+'" title="'+child_element[j].title+'">'+child_element[j].title+'</a></li>'
        menu +='</ul></div>'
        menu_block += menu
    return menu_block
