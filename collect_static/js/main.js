$(document).ready(function(){
	$("#price").click(function(){
		var data = $("#work_price").serialize();
		$.ajax({
			url:$("#work_price").attr("action") + "?" + data,
			type:'GET',
			success:function(data){
				if (data=='error'){
					$(".error").html("Вы заполнили не все поля");
				}
				if (data=='ok'){
					$("#work_price").html("<div class='thanks'>Спасибо.<br> Вам позвонят в ближайшее время</div>");
				}
			}
		});
	});

	$(document).ajaxStart(function(){
	    $("#loader").show();
	  
	});
	$(document).ajaxComplete(function(){
	    $("#loader").hide();
	});

	//bx-slider
	$('.bxslider').bxSlider({
		pager: true,
        auto: true
	});


	$(".diplom_h").live("click",function(){
		var div_status = $(this).next(".hide_div").css("display");
		if (div_status == 'none'){
			$(this).next(".hide_div").fadeIn();
		}else{
			$(this).next(".hide_div").fadeOut();
		}
	});
	var placeholder_ar = $(".placeholder"); 
	placeholder_ar.each(function(index,value){
		$(this).children("input").attr('placeholder','заполните поле');
	});

});