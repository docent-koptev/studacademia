# -*- coding: utf-8 -*-
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
LOCAL_SETTINGS = True
THUMBNAIL_DEBUG = True
# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'yxfn1+n#8(redo+lc8z&=b348f+g%#g2if+32!%o-x$=ugtc21'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG  = False

TEMPLATE_DEBUG = True
TEMPLATE_DIRS = (
    BASE_DIR + '/templates',
)

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = (
    #'admin_tools',,
    #'admin_tools.menu',
   # 'admin_tools.dashboard',
    'grappelli.dashboard',
    'grappelli',
    'filebrowser',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.sitemaps',
    'django.contrib.staticfiles',
    'django.contrib.flatpages',
    'django.contrib.sites',
    #--DJANGO-BATTERY--#
    'south',
    'sorl.thumbnail',
    'flatblocks',
    #'rollyourown.seo',
    #--VISIT-SITE-APPS-#
    'indexpage',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
)
SITE_ID = 1
ROOT_URLCONF = 'visitsite.urls'
WSGI_APPLICATION = 'visitsite.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'studacademia',
        'USER':'akoptev',
        'PASSWORD':'mosenkova',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'ru-Ru' 
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True


STATIC_ROOT = os.path.join(BASE_DIR, 'collect_static')
STATIC_URL = '/static/'

MEDIA_URL = '/media/'

STATICFILES_DIRS = (
    (os.path.join(BASE_DIR, 'static')),

)



TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    "django.core.context_processors.request"
)
#Where find static
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder'
)


#ADMIN_TOOLS_INDEX_DASHBOARD = 'dashboard.CustomIndexDashboard'
GRAPPELLI_INDEX_DASHBOARD = 'somefile.CustomIndexDashboard'
if LOCAL_SETTINGS:
    try:
        from local_settings import *
    except ImportError:
        pass

GRAPPELLI_ADMIN_TITLE = 'PLACE-START admin'
EMAIL_HOST = 'smtp.webfaction.com'
EMAIL_PORT = 25
EMAIL_HOST_USER = 'akoptev'
EMAIL_HOST_PASSWORD = 'ak12345'
EMAIL_USE_TLS = False

SERVER_EMAIL = 'no-reply@studacademia.ru'
DEFAULT_FROM_EMAIL = 'no-reply@studacademia.ru'

CPU_TIME_LIMIT = 20