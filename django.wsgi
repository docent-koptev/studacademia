import os
import sys

sys.path.insert(0, '/home/akoptev/webapps/studacademia/studenv/lib/python2.7/site-packages')
sys.path.insert(0, '/home/akoptev/webapps/studacademia/studacademia')

from django.core.handlers.wsgi import WSGIHandler

os.environ['DJANGO_SETTINGS_MODULE'] = 'visitsite.settings'
application = WSGIHandler()
