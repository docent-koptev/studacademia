# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from grappelli.dashboard import modules, Dashboard
from grappelli.dashboard.utils import get_admin_site_name


class CustomIndexDashboard(Dashboard):
    def init_with_context(self, context):
        site_name = get_admin_site_name(context)
        
        # append a group for "Administration" & "Applications"
        self.children.append(modules.ModelList(
            title=u'Заказы',
            column=2,
            collapsible=True,
            models=('indexpage.models.UpgradeUnic','indexpage.models.ElectroDocument','indexpage.models.TranslateBlank','indexpage.models.StudWork',),
        ))
        
        self.children.append(modules.ModelList(
            title=u'Присланные анкеты',
            column=2,
            collapsible=True,
            models=('indexpage.models.Vakansy','indexpage.models.Translate','indexpage.models.Author','indexpage.models.Manager',),
        ))

        self.children.append(modules.ModelList(
            title=u'Предлагаемые мной услуги',
            column=1,
            models=('indexpage.models.Uslugi','indexpage.models.Work'),
        ))

        self.children.append(modules.ModelList(
            title=u'Настройки сайта и пользователей',
            column=1,
            models=('django.contrib.sites.*','django.contrib.auth.*',),
        ))
        

        self.children.append(modules.Group(
            title="Верхнее и нижнее меню",
            column=1,
            collapsible=True,
            children=[
                modules.ModelList(
                    title=u'Верхнее меню',
                    column=1,
                    models=('django.contrib.flatpages.models.FlatPage',),
                ),
                modules.ModelList(
                    title=u'Нижнее меню с категориями',
                    column=1,
                    models=('indexpage.models.Parent','indexpage.models.Menubottom',),
                )
            ]
        ))

        self.children.append(modules.ModelList(
            title=u'Слайдер на главной',
            column=3,
            models=('indexpage.models.Slider', ),
        ))

        self.children.append(modules.ModelList(
            title=u'СЕО',
            column=1,
            models=('rollyourown.seo.*', ),
        ))

